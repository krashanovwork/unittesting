﻿//## The FizzBuzz Kata

//*Write a program that prints the numbers from 1 to 100.
//* But for multiples of three print "Fizz" instead of the number and for the multiples of five print "Buzz".
//* For numbers which are multiples of both three and five print "FizzBuzz".

//#### Steps:

//Lets divide this into different steps so, we can easily write and test this.

//* Print numbers from 1 to 100
//* Print "Fizz" instead of number which is divisible by 3
//* Print "Buzz" instead of number which is divisible by 5
//* Print "FizzBuzz" instead of number which is divisible by both 3 and 5

//#### Make more test for accept numbers and provide results

//* Create a method to accept single number
//* Create test to verify supplied number within the range 1 to 100
//* Create test to verify number and return result Fizz or Buzz or FizzBuzz per above criteria

//#### References


namespace FizzBuzz
{
    public class FizzBuzzKata
    {
        public string CheckForFizzBuzz(int number)
        {
            string result = null;

            if (number < 1 || number > 100)
            {
                throw new InvalidOperationException();
            }
            else
            {
                if (number % 3 == 0 && number % 5 == 0)
                {
                    result = "FizzBuzz";
                }
                else if (number % 3 == 0)
                {
                    result = "Fizz";
                }
                else if (number % 5 == 0)
                {
                    result = "Buzz";
                }
                else
                {
                    result = number.ToString();
                }
            }

            return result;
        }
    }
}